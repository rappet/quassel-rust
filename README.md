[![Build Status](https://drone.rappet.de/api/badges/rappet/quassel-rust/status.svg)](https://drone.rappet.de/rappet/quassel-rust)

# Quassel Rust

This is a rust library for the quassel framing protocol.

## Quassel Protocol

A short documentation is [avaliable](proto.md) in this
repository which is based of
https://quasseldroid.info/docs/protocol/layers/framing/.