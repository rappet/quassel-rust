//! Type for handshake flags
//!
//! Type for Flags to determine if the connection should be encrypted or compressed

/// Wrapper for flags during protocol handshake.
///
/// # Flags
///
/// |  Flag  | Meaning             |
/// | ------ | ------------------- |
/// | `0x01` | TLS                 |
/// | `0x02` | Deflate-Compression |
///
/// # Examples
///
/// Parsing flags byte and returning used flags:
///
/// ```rust
/// use quassel_rust::HandshakeFlags;
///
/// fn print_flags(flags: u8) {
///     let flags: HandshakeFlags = flags.into();
///
///     println!("Flags: tls: {}, deflate: {}", flags.tls(), flags.deflate());
/// }
///
/// print_flags(0x03);
/// ```
///
/// Creating flags:
///
/// ```rust
/// use quassel_rust::HandshakeFlags;
///
/// fn tls_only() -> u8 {
///     HandshakeFlags::new().with_tls().into()
/// }
///
/// let _ = tls_only();
/// ```
#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub struct HandshakeFlags(u8);

impl HandshakeFlags {
    pub fn new() -> Self {
        Self::default()
    }

    /// creates the magic number to open a connection
    pub fn magic(self) -> u32 {
        self.0 as u32 | 0x42b33f00
    }

    pub fn tls(self) -> bool {
        self.0 & 0x01 != 0
    }

    pub fn deflate(self) -> bool {
        self.0 & 0x02 != 0
    }

    pub fn with_tls(self) -> Self {
        HandshakeFlags(self.0 | 0x01)
    }

    pub fn with_deflate(self) -> Self {
        HandshakeFlags(self.0 | 0x02)
    }
}

impl From<u8> for HandshakeFlags {
    fn from(flags: u8) -> HandshakeFlags {
        HandshakeFlags(flags)
    }
}

impl From<HandshakeFlags> for u8 {
    fn from(flags: HandshakeFlags) -> u8 {
        flags.0
    }
}

impl Default for HandshakeFlags {
    fn default() -> Self {
        HandshakeFlags(0u8)
    }
}

#[cfg(test)]
mod test {
    use crate::HandshakeFlags;

    #[test]
    fn test_flags_into() {
        let flags: HandshakeFlags = 0x03.into();
        assert!(flags.tls() == true);
        assert!(flags.deflate() == true);
    }

    #[test]
    fn test_flags_build() {
        let flags = HandshakeFlags::new().with_tls().with_deflate();
        assert!(flags.tls() == true);
        assert!(flags.deflate() == true);
    }
}