# Quassel Protocol

*This document is based on the [quasseldroid documentation][qd-framing]*

This document describes the quassel protocol v2,
called framing protocol.

## State

A connection can be divided in three parts:

- protocol hanshake
- application handshake
- connected session

## Protocol handshake

The protocol handshake is the initial part of an connection
and negotiated which protocol version should be used
and if compression or TLS is supported and
should be used.

### Client hanshake

Data is transmitted in network byte order.

```
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|           Magic Number 0x42b33f               |      Flags    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|0|            Protocol 1                                       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|0|            Protocol 2                                       |

....

|1|            Protocol n                                       |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

#### Flags

|  Flag  | Meaning             |
| ------ | ------------------- |
| `0x01` | TLS                 |
| `0x02` | Deflate-Compression |

#### Supported Protocol

Version | Protocol
------- | --------
`0x01`  | Old legacy protocol, which won’t be described here
`0x02`  | Datastream protocol, as described in this document

The last protocol is marked with the `0x80000000` flag.
quassel-rust does only implement the newer datastream protocol.

### Server handshake

```
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Flags           | Reserved                      | Protocol    |
|                 |                               | Version     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

## Framing Protocol

Depending on flags,
after the handshake a TLS connection is optional established.
That connection is optionaly compressed with a Deflate stream.
In that stream the data is transmitted in frames.
After each frame the connection should be flushed.

A frame consists of a `u32` with the `length` of the data
and the `length` bytes of data.

## Application handshake

## Connected session

[qd-framing]: https://quasseldroid.info/docs/protocol/layers/framing/