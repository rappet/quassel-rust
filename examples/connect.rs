#[macro_use]
extern crate log;

use std::{
    env,
    io,
    net::TcpStream,
};

use byteorder::{ReadBytesExt, WriteBytesExt, BigEndian};

use quassel_rust::HandshakeFlags;
use std::net::{SocketAddr, ToSocketAddrs};
use bufstream::BufStream;
use std::io::Write;

fn main() -> io::Result<()> {
    env::set_var("RUST_LOG", "connect=info");
    env_logger::init();

    let destination: SocketAddr = env::args().nth(1)
        .unwrap_or("127.0.0.1:4242".to_string())
        .to_socket_addrs()?
        .next().ok_or(io::Error::new(io::ErrorKind::NotFound, "could not resolve host"))?;
    let mut stream = BufStream::new(TcpStream::connect(&destination)?);

    stream.write_u32::<BigEndian>(HandshakeFlags::new().with_tls().magic())?;
    stream.write_u32::<BigEndian>(0x80000002)?;
    stream.flush()?;

    let flags: HandshakeFlags = stream.read_u8()?.into();
    info!("Connection flags: tls: {}, deflate: {}", flags.tls(), flags.deflate());
    let _reserved = stream.read_u16::<BigEndian>()?;
    let protocol_version = stream.read_u8()?;
    info!("Protocol version: {}", protocol_version);

    Ok(())
}